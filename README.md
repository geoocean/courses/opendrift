# Analysis of Surface Water Contamination Risk
GeoOcean Engineering Group - Departamento de Ciencias y Técnicas del Agua y del Medioambiente
Universidad de Cantabria

## Install

The source code is currently hosted on GitLab at [this link](https://gitlab.com/geoocean/courses/opendrift)

## Folders

### Environment installation

Both environment and requirements can be installed navigating to the base root of [opendrift](./) and typing:

```
    # type in terminal
    conda env create -f environment.yml 
```
To install OpenDrift, navigate to [https://opendrift.github.io/](https://opendrift.github.io/)
## Launch Jupyter Lab

Activate the environment and launch jupyter lab:

```
    # type in terminal
    conda activate opendrift
    jupyter lab
```
## Prerequisites

Download the lastest stable version of OpenDrift [clicking here](https://opendrift.github.io/install.html)


## License

This project is licensed under the MIT License - see the [license](./LICENSE.txt) file for more details
