
import os
import os.path as op
import sys

import numpy as np
import xarray as xr
import pyproj
import pandas as pd
from shapely.geometry import box, Point
import geopandas as gpd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.colors import LinearSegmentedColormap
import contextily as ctx

matplotlib.rcParams['animation.embed_limit'] = 2**128

def find_index_of_nearest_xy(y_array, x_array, y_point, x_point):
    distance = (y_array-y_point)**2 + (x_array-x_point)**2
    ids = np.where(distance==distance.min())[0][0]
    return ids

def convertir_a_utm(lat, lon):
    # Definir el sistema de coordenadas de origen (latitud, longitud)
    crs_wgs = pyproj.CRS('EPSG:4326')  # WGS84

    # Calcular la zona UTM
    zona_utm = int((lon + 180) / 6) + 1

    # Definir el sistema de coordenadas de destino (UTM)
    crs_utm = pyproj.CRS(f'EPSG:326{zona_utm:02d}')  # UTM correspondiente a la zona

    # Crear el transformador de coordenadas
    transformer = pyproj.Transformer.from_crs(crs_wgs, crs_utm, always_xy=True)

    # Convertir las coordenadas a UTM
    x, y = transformer.transform(lon, lat)

    return x, y, zona_utm


def convertir_a_lat_lon(x, y, zona_utm):
    
    # Definir el sistema de coordenadas de origen (UTM)
    crs_utm = pyproj.CRS(f'EPSG:326{zona_utm:02d}')  # UTM correspondiente a la zona

    # Definir el sistema de coordenadas de destino (latitud, longitud)
    crs_wgs = pyproj.CRS('EPSG:4326')  # WGS84

    # Crear el transformador de coordenadas
    transformer = pyproj.Transformer.from_crs(crs_utm, crs_wgs, always_xy=True)

    # Convertir las coordenadas a latitud, longitud
    lon, lat = transformer.transform(x, y)

    return lat, lon


def oil_animation(ds_in, ds_out, coast, shp, P1, P2, P3, P4, p = [], radio=None):
    
    extent = [ds_in.x.values[0], ds_in.x.values[-1], np.min(ds_in.y.values), np.max(ds_in.y.values)]

    cmap = plt.get_cmap('rainbow')
    colors = cmap(np.linspace(0, 1, 20))
    colors[:, -1] = np.linspace(0, 1, 20)
    cmap1 = LinearSegmentedColormap.from_list('cmap', colors, N=20)

    fig, ax = plt.subplots()

    ax.axis(extent)
    ax.set_title('{0}'.format(pd.to_datetime(ds_in.time.values[0]).strftime('%m-%d %H:%M')))
    ax.scatter(coast['lon'], coast['lat'], s=0.05, c='grey')
    ctx.add_basemap(ax, crs='EPSG:4326', source=ctx.providers.OpenStreetMap.Mapnik, zoom=13, attribution=False)
    shp.plot(ax=ax, fc='w', ec=None)
    
    xdata, ydata = [], []
    ln, = ax.plot([], [], 'ok', markersize=1)
    cax = ax.pcolormesh(ds_in.x, ds_in.y, ds_in.isel(time=1).sea_water_velocity, 
                        vmin=0, vmax=ds_in.sea_water_velocity.max(), cmap=cmap1, zorder=1)

    Q = ax.quiver(ds_in.x.values, ds_in.y.values, 
                  ds_in.isel(time=0).x_sea_water_velocity.values, 
                  ds_in.isel(time=0).y_sea_water_velocity.values, 
                  color='darkgrey', zorder=2, scale=7, headlength=5, units='width')

    cb = fig.colorbar(cax, shrink=0.7)
    cb.set_label('Velocidad Corriente (m/s)')
    ax.set_xlabel('Longitud')
    ax.set_ylabel('Latitud')
    ax.scatter(P1[0], P1[1], s=4, c='k', zorder=10)
    ax.scatter(P2[0], P2[1], s=4, c='k', zorder=10)
    ax.scatter(P3[0], P3[1], s=4, c='k', zorder=10)
    ax.scatter(P4[0], P4[1], s=4, c='k', zorder=10)
    
    
    if radio is not None:
        square = box(extent[0], extent[1], extent[2], extent[3])
        center = Point(p)
        radius = radio
        circle = center.buffer(radius)
        intersection = square.intersection(circle)
        difference = square.difference(intersection)
        
        gdf = gpd.GeoDataFrame(geometry=[difference])
        xmin, ymin, xmax, ymax = gdf.total_bounds
        
        gdf.plot(ax=ax, fc='w', ec='grey', zorder=11)
        ax.set_xlim(p[0]-radio, p[0]+radio)
        ax.set_ylim(p[1]-radio, p[1]+radio)
        ax.axis('off')
        
    else:
        extent = [ds_in.x.values[0], ds_in.x.values[-1], np.min(ds_in.y.values), np.max(ds_in.y.values)]

    plt.close()

    def update(frame):
        ax.cla()
        ax.axis(extent)

        ax.cla()
        
        ax.set_title('{0}'.format(pd.to_datetime(ds_out.time.values[frame]).strftime('Día: %d Hora: %H Minuto: %M')))
        ax.scatter(coast['lon'], coast['lat'], s=0.05, c='grey')

        ax.axis(extent)
        ctx.add_basemap(ax, crs='EPSG:4326', source=ctx.providers.OpenStreetMap.Mapnik, zoom=13, attribution=False)
        shp.plot(ax=ax, fc='w', ec=None)
        ax.pcolormesh(ds_in.x, ds_in.y, ds_in.isel(time=frame).sea_water_velocity, 
                        vmin=0, vmax=ds_in.sea_water_velocity.max(), cmap=cmap1, zorder=1)

        ax.quiver(ds_in.x.values, ds_in.y.values, 
                  ds_in.isel(time=frame).x_sea_water_velocity.values, 
                  ds_in.isel(time=frame).y_sea_water_velocity.values, 
                  color='darkgrey', zorder=2,  scale=7, headlength=5, units='width')

        xds = ds_out.isel(time=frame).to_dataframe()
        xds_moving = xds.loc[xds['moving'] == 1]
        xds_sl = ds_out.isel(time=slice(0, frame)).to_dataframe()
        xds_stranded = xds_sl.loc[xds_sl['moving'] == 0]

        ax.scatter(ds_out.isel(time=0).lon, ds_out.isel(time=0).lat, 
                   label='inicial ({0})'.format(len(ds_out.isel(time=0).lat)), s=1, c='olive')

        ax.scatter(xds_moving.lon.values, xds_moving.lat.values, 
                   s=1, c='slateblue', zorder=5, label='activas ({0})'.format(len(xds_moving)))

        ax.scatter(xds_stranded.lon.values, xds_stranded.lat.values, 
                   s=1, c='orangered', zorder=5, label='varadas ({0})'.format(len(xds_stranded)))
    
    
        if radio:
            ax.scatter(p[0], p[1], s=2, c='k', zorder=10)
            ax.text(p[0], p[1], 'P', zorder=10)
            
            gdf.plot(ax=ax, fc='w', ec='grey', zorder=15)
            ax.scatter(p[0], p[1], s=10,  c='k')
            ax.set_xlim([p[0]-radio, p[0]+radio])
            ax.set_ylim([p[1]-radio, p[1]+radio])
            ax.axis('off')
        
        else:
            for ppoint, point in enumerate([P1, P2, P3, P4]):
                ax.scatter(point[0], point[1], s=2, c='k', zorder=10)
                ax.text(point[0], point[1], 'P{0}'.format(ppoint+1), zorder=10)
            
        legend = ax.legend(loc='lower right')
        for i in range(len(legend.legendHandles)):
            legend.legendHandles[i]._sizes = [30]
        legend.set_zorder(20)
            
        return (cax, Q)

    ani = FuncAnimation(fig, update, frames=len(ds_out.time),
                        blit=True, interval=600)

    
    return(ani)
